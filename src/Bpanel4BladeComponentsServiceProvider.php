<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BladeComponents;

use Bittacora\Bpanel4\BladeComponents\View\Components\Info;
use Bittacora\Bpanel4\BladeComponents\View\Components\Warning;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;

class Bpanel4BladeComponentsServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        parent::register();
    }

    public function boot(BladeCompiler $blade): void
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'bpanel4-blade-components');
        $blade->component('bpanel4-warning', Warning::class);
        $blade->component('bpanel4-info', Info::class);
        parent::register();
    }
}
