<?php

namespace Bittacora\Bpanel4\BladeComponents\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Warning extends Component
{
    public $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function render()
    {
        return view('bpanel4-blade-components::warning');
    }
}
