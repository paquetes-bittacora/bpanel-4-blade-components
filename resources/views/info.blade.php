<div>
    <div role="alert" class="alert alert-info bgc-info-l4 brc-info-m3 border-2 d-flex align-items-center">
        <i class="fas fa-info-circle mr-3 fa-2x text-blue"></i>

        <div class="text-dark-tp2">
            {{ $message }}
        </div>

    </div>
</div>
