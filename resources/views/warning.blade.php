<div>
    <div role="alert" class="alert alert-warning bgc-warning-l4 brc-warning-m3 border-2 d-flex align-items-center">
        <i class="fas fa-exclamation-circle mr-3 fa-2x text-orange"></i>

        <div class="text-dark-tp2">
            {{ $message }}
        </div>

    </div>
</div>
