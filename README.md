# bPanel 4 blade components

Componentes de blade para facilitar el uso de algunos componentes de Ace 4 en bPanel 4.

# Componentes

## Warning

Muestra un mensaje tipo "warning":

**Ejemplo:**

![warning](./docs/images/warning.png)

**Código:**
```html
<x-bpanel4-warning message="Para añadir imágenes al producto, primero debe guardarlo."/>
```

## Info

Muestra un aviso informativo:

**Ejemplo:**

![warning](./docs/images/info.png)

**Código:**
```html
<x-bpanel4-info message="Lorem ipsum dolor sit amet, consectetur adipiscing elit."/>
```
